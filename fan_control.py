#!/usr/bin/python3
import PID
import time
import os.path
import os

targetT = 40
P = 10
I = 1
D = 1

lower_temp_limit = 40
pid = PID.PID(P, I, D)
pid.SetPoint = targetT
pid.setSampleTime(1)

def readConfig ():
	global targetT
	with open ('/etc/pid/pid.conf', 'r') as f:
		config = f.readline().split(',')
		pid.SetPoint = float(config[0])
		targetT = pid.SetPoint
		pid.setKp (float(config[1]))
		pid.setKi (float(config[2]))
		pid.setKd (float(config[3]))

def createConfig ():
	if not os.path.isfile('/etc/pid/pid.conf'):
		with open ('/etc/pid/pid.conf', 'w') as f:
			f.write('%s,%s,%s,%s'%(targetT,P,I,D))

createConfig()
# system files

cpu_file = "/sys/class/thermal/thermal_zone0/temp"
pwm_file = "/sys/class/hwmon/hwmon0/pwm1"

def main():
	while 1:
		readConfig()
		cpu = open(cpu_file, "r")
		pwm = open(pwm_file, "w")
		temp = float(int(cpu.read()) / 1000)
		cpu.close()
		if temp < 10:
			pwm.write(str(60))
			pid.update(temp)
			print("below low limit, pid not active")
		else:
			pid.update(temp)
			targetPwm = pid.output
#                        print(targetPwm)
			targetPwm = max(min( int(targetPwm), 255 ),85)
#                        print("Target: %.1f C | Current: %.1f C | PWM: %s "%(targetT, temp, targetPwm))
                        # Set PWM
			new_pwm = int((targetPwm/100*255))
#                        print(targetPwm)
			pwm.write(str(targetPwm))
		pwm.close()
		time.sleep(0.5)

if __name__ == '__main__':
    main()

